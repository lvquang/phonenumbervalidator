//
//  ViewController.h
//  PhoneNumberValidator
//
//  Created by Quang Luu Vinh on 6/10/14.
//  Copyright (c) 2014 Quang Luu Vinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NBPhoneNumberUtil.h"
#import "CountryPicker.h"
#import "Defines.h"
#import "PhoneNumber.h"

@interface ViewController : UIViewController <CountryPickerDelegate, UITextFieldDelegate>{
    IBOutlet UIPickerView *viewCountryPicker;
    IBOutlet UITextField *tfPhoneNumber;
    
    IBOutlet UILabel *lbLocation;
    IBOutlet UILabel *lbCountry;
    IBOutlet UILabel *lbCountryCode;
    IBOutlet UILabel *lbNationalNumber;
    IBOutlet UILabel *lbIntertionalNumber;
    
    NSString *countryName;
    NSString *countryCode;
    
    UITapGestureRecognizer *singleTap;  
    NBPhoneNumberUtil *phoneUtil;
    
    PhoneNumber *phoneNumber;
    NBPhoneNumber *myNumber;
}

@end
