//
//  PhoneNumber.h
//  PhoneNumberValidator
//
//  Created by Quang Luu Vinh on 6/10/14.
//  Copyright (c) 2014 Quang Luu Vinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhoneNumber : NSObject

@property (nonatomic, strong) NSString *location; //VietNam
@property (nonatomic, strong) NSString *country; //VN
@property (nonatomic, strong) NSString *countryCode; //84
@property (nonatomic, strong) NSString *nationalNumber;
@property (nonatomic, strong) NSString *internationalNumber;

- (id)initPhoneNumberWith:(NSString *)_location andCountry:(NSString *)_country andCountryCode:(NSString *)_countryCode andNationalNumber:(NSString *)_nationalNumber andInterNationalNumber:(NSString *)_internationalNumber;

@end
