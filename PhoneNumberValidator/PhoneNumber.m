//
//  PhoneNumber.m
//  PhoneNumberValidator
//
//  Created by Quang Luu Vinh on 6/10/14.
//  Copyright (c) 2014 Quang Luu Vinh. All rights reserved.
//

#import "PhoneNumber.h"

@implementation PhoneNumber
@synthesize location, country, countryCode, nationalNumber, internationalNumber;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.location = @"-";
        self.country = @"-";
        self.countryCode = @"-";
        self.nationalNumber = @"-";
        self.internationalNumber = @"-";
    }
    return self;
}

- (id)initPhoneNumberWith:(NSString *)_location andCountry:(NSString *)_country andCountryCode:(NSString *)_countryCode andNationalNumber:(NSString *)_nationalNumber andInterNationalNumber:(NSString *)_internationalNumber{
    self = [super init];
    
    if (self) {
        self.location = _location;
        self.country = _country;
        self.countryCode = _countryCode;
        self.nationalNumber = _nationalNumber;
        self.internationalNumber = _internationalNumber;
    }
    return self;
}

@end
