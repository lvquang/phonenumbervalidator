//
//  ViewController.m
//  PhoneNumberValidator
//
//  Created by Quang Luu Vinh on 6/10/14.
//  Copyright (c) 2014 Quang Luu Vinh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self checkAndResizeLayout];
    singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureRecognizer:)];
    countryCode = @"AF";
    phoneUtil = [NBPhoneNumberUtil sharedInstance];
    phoneNumber = [[PhoneNumber alloc]init];
}

#pragma mark -
#pragma mark - phone number checking
// -------------------------------------------------------------------------------
//	validatePhoneNumber:
//  Phone number is only valid if it is valid with current country code
// -------------------------------------------------------------------------------
- (IBAction)validatePhoneNumber:(id)sender{
    NSString *strPhoneNumero = [[tfPhoneNumber text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSError *aError = nil;
    UIAlertView *alert;
    
    if ([strPhoneNumero length] == 0) {
        alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Phone number is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else{
        myNumber = [phoneUtil parse:strPhoneNumero defaultRegion:countryCode error:&aError];
        if (!aError) {
            if ([phoneUtil isValidNumber:myNumber]) {
                NSString *tempNationalCode = [phoneUtil getRegionCodeForNumber:myNumber];
                if ([tempNationalCode isEqualToString:countryCode]) {
                    alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your phone number is valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [self setDataToPhoneNumber:phoneNumber];
                }else{
                    // number is valid but not valid with current country code.
                    alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your phone number isn't valid with this country. Please try another one." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                }
            }else{
                // number is not valid
                alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your phone number is not valid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
        }
        else{
            alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:[aError localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            NSLog(@"Some weird errors happend while parsing number: %@", [aError localizedDescription]);
        }
    }
    [self showInfoOfPhoneNumber:phoneNumber];
    [alert show];
}

#pragma mark -
#pragma mark - countryPickerDelegate
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    countryName = name;
    countryCode = code;
}

#pragma mark -
#pragma mark - UITapGestureRecognizer
-(void)tapGestureRecognizer:(UIGestureRecognizer*)gesture{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [self.view removeGestureRecognizer:singleTap];
};

#pragma mark -
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.view addGestureRecognizer:singleTap];
    return YES;
}

#pragma mark -
#pragma mark - checkAndResizeLayout
- (void)checkAndResizeLayout{
    if (IS_IPHONE_5) {
        CGRect rect = viewCountryPicker.frame;
        rect.origin.y += HEIGHT_DELTA_VIEW;
        viewCountryPicker.frame = rect;
    }
}


#pragma mark -
#pragma mark - Get/show data of phone number

// -------------------------------------------------------------------------------
//	setDataToPhoneNumber:
// -------------------------------------------------------------------------------
-(void)setDataToPhoneNumber:(PhoneNumber *)_phoneNumber{
    if (_phoneNumber) {
        NSString *location = countryName;
        NSString *country = [phoneUtil getRegionCodeForNumber:myNumber];
        NSString *code = [[phoneUtil getCountryCodeForRegion:country]stringValue];
        NSString *nationalNumber = [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatNATIONAL error:nil];
        NSString *internationalNumber = [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:nil];
        _phoneNumber = [_phoneNumber initPhoneNumberWith:location andCountry:country andCountryCode:code andNationalNumber:nationalNumber andInterNationalNumber:internationalNumber];
    }
}

// -------------------------------------------------------------------------------
//	showInfoOfPhoneNumber:
// -------------------------------------------------------------------------------
- (void)showInfoOfPhoneNumber:(PhoneNumber *)_phoneNumber{
    if (_phoneNumber) {
        [lbLocation setText:[phoneNumber location]];
        [lbCountry setText:[phoneNumber country]];
        [lbCountryCode setText:[phoneNumber countryCode]];
        [lbNationalNumber setText:[phoneNumber nationalNumber]];
        [lbIntertionalNumber setText:[phoneNumber internationalNumber]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
