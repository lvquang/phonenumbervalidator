//
//  AppDelegate.h
//  PhoneNumberValidator
//
//  Created by Quang Luu Vinh on 6/10/14.
//  Copyright (c) 2014 Quang Luu Vinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
